const Berita = require('../models/beritaModel.js');
exports.findAll = function(req, res) {
    Berita.findAll(function(err, berita) {
        console.log('controller')
        if (err)
            res.send(err);
        console.log('res', berita);
        res.send(berita);
    });
};
exports.create = function(req, res) {
    const new_berita = new Berita(req.body);
//handles null error
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Please provide all required field' });
    }else{
        Berita.create(new_berita, function(err, berita) {
            if (err)
                res.send(err);
            res.json({error:false,message:"Berita added successfully!",data:berita});
        });
    }
};
exports.findById = function(req, res) {
    Berita.findById(req.params.id, function(err, berita) {
        if (err)
            res.send(err);
        res.json(berita);
    });
};
exports.update = function(req, res) {
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.status(400).send({ error:true, message: 'Please provide all required field' });
    }else{
        Berita.update(req.params.id, new Berita(req.body), function(err, berita) {
            if (err)
                res.send(err);
            res.json({ error:false, message: 'Berita successfully updated' });
        });
    }
};
exports.delete = function(req, res) {
    Berita.delete( req.params.id, function(err, berita) {
        if (err)
            res.send(err);
        res.json({ error:false, message: 'Berita successfully deleted' });
    });
};