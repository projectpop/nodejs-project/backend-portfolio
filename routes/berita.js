const express = require('express')
const router = express.Router()
const beritaController =   require('../controllers/beritaController.js');
const jwt = require('jsonwebtoken');

router.use(function (req, res, next) {
    try {
        const token = req.headers.token;
        let decoded = jwt.verify(token, process.env.SECRET);
        req.user = decoded;
        next();
    } catch(err) {
        res.status(401).json({
            message: 'Token is Invalid'
        });
    }
})

// Retrieve all berita
router.get('/', beritaController.findAll);
// Create a new berita
router.post('/', beritaController.create);
// Retrieve a single berita with id
router.get('/:id', beritaController.findById);
// Update a berita with id
router.put('/:id', beritaController.update);
// Delete a berita with id
router.delete('/:id', beritaController.delete);
module.exports = router