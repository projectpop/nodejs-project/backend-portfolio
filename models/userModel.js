let dbConn = require('../config/db.js');
const bcrypt = require('bcrypt');

//User object create
let User = function(user){
    this.name     = user.name;
    this.email      = user.email;
    this.password     = bcrypt.hashSync(user.password,10);
    this.role      = user.role;
    this.createdAt     = new Date();
    this.updatedAt     = new Date();
};
User.create = function (newEmp, result) {
    dbConn.query("INSERT INTO user set ?", newEmp, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
};
User.findById = function (id, result) {
    dbConn.query("Select * from user where id = ? ", id, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};
User.findAll = function (result) {
    dbConn.query("Select * from user", function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('user : ', res);
            result(null, res);
        }
    });
};
User.update = function(id, user, result){
    dbConn.query("UPDATE user SET name=? WHERE id = ?", [user.name, id], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }else{
            result(null, res);
        }
    });
};
User.delete = function(id, result){
    dbConn.query("DELETE FROM user WHERE id = ?", [id], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            result(null, res);
        }
    });
};

User.login = function(data, result){
    dbConn.query("SELECT * FROM user WHERE email = ?", [data.email], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }else{
            result(null, res[0]);
        }
    });
};

module.exports= User;