let dbConn = require('../config/db.js');
//Berita object create
let Berita = function(berita){
    this.judul     = berita.judul;
    this.konten      = berita.konten;
    this.createdAt     = new Date();
    this.updatedAt     = new Date();
};
Berita.create = function (newEmp, result) {
    dbConn.query("INSERT INTO berita set ?", newEmp, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            console.log(res.insertId);
            result(null, res.insertId);
        }
    });
};
Berita.findById = function (id, result) {
    dbConn.query("Select * from berita where id = ? ", id, function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else{
            result(null, res);
        }
    });
};
Berita.findAll = function (result) {
    dbConn.query("Select * from berita", function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            console.log('berita : ', res);
            result(null, res);
        }
    });
};
Berita.update = function(id, berita, result){
    dbConn.query("UPDATE berita SET judul=?,konten=? WHERE id = ?", [berita.judul,berita.konten, id], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }else{
            result(null, res);
        }
    });
};
Berita.delete = function(id, result){
    dbConn.query("DELETE FROM berita WHERE id = ?", [id], function (err, res) {
        if(err) {
            console.log("error: ", err);
            result(null, err);
        }
        else{
            result(null, res);
        }
    });
};
module.exports= Berita;