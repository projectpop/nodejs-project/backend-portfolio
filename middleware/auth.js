const jwt = require('jsonwebtoken');

exports.isAuth = function(req, res, next) {
    try {
        const token = req.headers.token;
        var decoded = jwt.verify(token, process.env.SECRET);
        req.user = decoded;
        next();
    } catch(err) {
        res.status(401).json({
            message: 'Token is Invalid'
        });
    }
};